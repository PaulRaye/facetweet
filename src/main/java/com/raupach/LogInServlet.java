package com.raupach;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(
    name = "LogInServlet",
    urlPatterns = {"/"}
)

/*
 * The login Servlet is the root of the site.  All it does is call the 
 * login.jsp view.
 */

public class LogInServlet extends HttpServlet {

  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response) 
      throws IOException {
	  
	doBoth(request, response);
  }

  @Override
  public void doPost(HttpServletRequest request, HttpServletResponse response) 
      throws IOException {
	  
	doBoth(request, response);
  }
  
  
  // Since we want both GET and POST requests to act the same.
  private void doBoth(HttpServletRequest request, HttpServletResponse response) throws IOException {
	try {
		request.getRequestDispatcher("log_in.jsp").forward(request, response);
	} catch (ServletException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}
}