package com.raupach;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.appengine.api.datastore.EntityNotFoundException;
import com.raupach.models.Tweet;
import com.raupach.models.User;

@WebServlet(
    name = "TweetDeleteServlet",
    urlPatterns = {"/tweet/delete"}
)

/*
 * The TweetDeleteServlet takes the request to delete a tweet 
 * deletes the tweet and returns the user the the tweets.jsp view.
 */

public class TweetDeleteServlet extends HttpServlet {

  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response) 
      throws IOException {
	  
	
	// Get the currently logged in user 
	User user;
	HttpSession session = request.getSession(true);
	try {
		user = new User((String) session.getAttribute("user_id"));
	} catch (EntityNotFoundException e) {
		user = null;
		e.printStackTrace();
	}
	request.setAttribute("user", user);
	
	
	// Delete the requested tweet
	// TODO make sure the tweet belongs to the current user
	Tweet.delete(request.getParameter("tweetID"));
	
	// Collect all the remaining tweets and hand them to the tweet.jsp
	ArrayList<Tweet> tweets = Tweet.usersTweets(user.getId(), false);
	request.setAttribute("tweets", tweets);
	
	try {
		request.getRequestDispatcher("../tweet.jsp").forward(request, response);
	} catch (ServletException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	

  }
  
}