package com.raupach;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.appengine.api.datastore.EntityNotFoundException;
import com.raupach.models.Tweet;
import com.raupach.models.User;

@WebServlet(
    name = "TweetServlet",
    urlPatterns = {"/tweet"}
)

/*
 * The Tweet Servlet 
 *  1) Takes the initial login information and saves the User // this should be moved
 *  2) Collects the user & user's tweets for display
 *  3) Takes the POST of new tweet and saves them. 
 */

public class TweetServlet extends HttpServlet {

  // the get request might be coming from two places
  // the navigation bar or the login screen
  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response) 
      throws IOException {
	// store the current user in a settion  
	// create a session object if it is already not created.
    HttpSession session = request.getSession(true);
    // get the session id and the param id for latter comparison
	String user_session_id = (String) session.getAttribute("user_id");
	String user_param_id = request.getParameter("userID");
    User user;
    
    // if this is a new session of the session id is null 
    // then we want to create/update and User.
    if (session.isNew() || user_session_id == null ) {
    	 user = new User(
    			 user_param_id,
    			 request.getParameter("first_name"),
    			 request.getParameter("last_name"),
    			 request.getParameter("email_address"));
    	 
    	 // save the new user in the session 
    	 session.setAttribute("user_id", user.getId());
    } else {
    	try {
    		// if there is a session then make sure it matches the session id
    		// if now create/update the current user
    		if (user_param_id != null && !user_session_id.equals(user_param_id )) {
    			user = new User(
    	    			 user_param_id,
    	    			 request.getParameter("first_name"),
    	    			 request.getParameter("last_name"),
    	    			 request.getParameter("email_address"));
    			// save the new user in the session
    			session.setAttribute("user_id", user.getId());
    		} else {
    			// otherwise just load up the session user. Whew
    			user = new User(user_session_id);
    		}
		} catch (EntityNotFoundException e) {
			user = null;
			e.printStackTrace();
		}
    }
	
	request.setAttribute("user", user);
	
	// collect the user's tweets and send them to the view
	ArrayList<Tweet> tweets = Tweet.usersTweets(user.getId(), false);
	request.setAttribute("tweets", tweets);

	try {
		request.getRequestDispatcher("tweet.jsp").forward(request, response);
	} catch (ServletException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	

  }

  
  // a Post request is a new tweet
@Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	// get the user id from the session
    HttpSession session = request.getSession(true);
	String user_id = (String) session.getAttribute("user_id");
	
	User user;
	Tweet tweet;
	
	try {
		// get the user
		user = new User(user_id);
		request.setAttribute("user", user);
		
		// get the users tweet before adding the new tweet because 
		// sometime the new tweet will not show up fast enough 
		// so we'll add it ourselves
		ArrayList<Tweet> tweets = Tweet.usersTweets(user.getId(), false);
		request.setAttribute("tweets", tweets);
		
		// create the new tweet and save it.
		tweet = new Tweet(request.getParameter("content"), user.getId(), user.getFullName());
		tweet.save();
		
		// add the new tweet to the top of the tweet list.
		tweets.add(0, tweet);
		
	} catch (EntityNotFoundException e1) {
		e1.printStackTrace();
	}
	
	try {
		request.getRequestDispatcher("tweet.jsp").forward(request, response);
	} catch (ServletException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}
  
  
}