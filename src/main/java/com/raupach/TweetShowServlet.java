package com.raupach;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.appengine.api.datastore.EntityNotFoundException;
import com.raupach.models.Tweet;
import com.raupach.models.User;

@WebServlet(
    name = "TweetShowServlet",
    urlPatterns = {"/tweet/show"}
)

/*
 * The show tweet get a tweet from storage and displays it in the 
 * tweet_show.jsp view.
 */

public class TweetShowServlet extends HttpServlet {

  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response) 
      throws IOException {
	  
	
	  // get the tweet from storage
	Tweet tweet;
	try {
		tweet = new Tweet(request.getParameter("tweetID"));
	} catch (EntityNotFoundException e1) {
		// TODO Auto-generated catch block
		tweet = null;
		e1.printStackTrace();
	}
	
	// hand the tweet to the view.
	request.setAttribute("tweet", tweet);
	
	try {
		request.getRequestDispatcher("../tweet_show.jsp").forward(request, response);
	} catch (ServletException e) {
		e.printStackTrace();
	}
	

  }
  
}