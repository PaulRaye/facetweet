package com.raupach;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.EntityNotFoundException;
import com.raupach.models.User;

@WebServlet(
    name = "FriendsServlet",
    urlPatterns = {"/friends"}
)


/* 
 * The Friend Servlet take the friends request and collects all the 
 * friend user objects in an array and hands them to the friends.jsp 
 * view.
 */

public class FriendsServlet extends HttpServlet {

  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response) 
      throws IOException {
	  
	// take the passed in friend user ids and split them into an array 
	String [] friend_ids = request.getParameter("friends").split(",");  
	ArrayList<User> friends = new ArrayList<User>();  
	
	// loop through the friend ids and collect the user objects
	for (String friend_id: friend_ids) {
		try {
			friends.add(new User(friend_id));
		} catch (EntityNotFoundException e) {
			// if the friend doesn't exist skip it
		}
	}
	
	// pass the friends to the friends view
	request.setAttribute("friends", friends);
	try {
		request.getRequestDispatcher("friends.jsp").forward(request, response);
	} catch (ServletException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	

  }
}