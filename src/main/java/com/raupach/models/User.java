package com.raupach.models;

import java.util.ArrayList;
import java.util.Date;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;


/* 
 * The User model handle the storage of User data and the creation of the User model objects
 */

public class User {
	private String id;					// user id, created in Facebook
	private String firstName;			// First name 
	private String lastName;			// Last name
	private String emailAddress;		// email address
	private Date lastUsed;				// not used in the end
	
	
	// ceates and stores a user from Facebook
	public User(String id, String firstName, String lastName, String emailAddress) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.emailAddress = emailAddress;
		this.lastUsed = new Date();
		save();
	}
	
	// Loads a user from storage
	public User(String id) throws EntityNotFoundException {
		super();
		this.id = id;
		load();
	}


	// saves a users properies to storage
	private void save() {
		
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		
		// Create the user entity
		Entity user = new Entity("User", this.id);
		user.setProperty("firstName", this.firstName);
		user.setProperty("lastName", this.lastName);
		user.setProperty("emailAddress", this.emailAddress);
		user.setProperty("lastUsed", this.lastUsed);
		// store it in the database
		ds.put(user);
		
	}
	
	// loads a user from storage
	private void load() throws EntityNotFoundException {
		// get the user from storage 
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Key k = KeyFactory.createKey("User", this.id); 
		Entity storedUser = datastore.get(k);
		
		// load in the user's properies
		this.firstName = (String) storedUser.getProperty("firstName");
		this.lastName = (String) storedUser.getProperty("lastName");
		this.emailAddress = (String) storedUser.getProperty("emailAddress");
		this.lastUsed = (Date) storedUser.getProperty("lastUsed");
		
		
	}

	//	getters and setters 
	
	// Returns all the tweets for this user
	public  ArrayList<Tweet> getTweets() {
		 return Tweet.usersTweets(this.id, true);
	}
	
	// returns the user's full name
	public String getFullName() {
		return firstName + " " + lastName;
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Date getLastUsed() {
		return lastUsed;
	}
	public void setLastUsed(Date lastUsed) {
		this.lastUsed = lastUsed;
	}
	
	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	
}
