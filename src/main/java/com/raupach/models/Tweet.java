package com.raupach.models;

import java.util.ArrayList;
import java.util.Date;

import org.junit.runner.FilterFactoryParams;


import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.PreparedQuery.TooManyResultsException;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.SortDirection;


/*
 * The Tweet Model handles collecting tweets from storage and creating tweet model objects.
 */
public class Tweet {
	
	private Long id;				// storage id
	private String content;			// content of the tweet
	private String user_id;			// user id of the author
	private String user_name;		// authors name
	private Date timestamp;			// creation timestamp
	private Long visited;			// visited count for the top tweets
	
	
	/* Class Methods */

	// userTweet takes a user id and returns all the users tweets
	public static ArrayList<Tweet> usersTweets(String user_id, Boolean markVisited) { 
		ArrayList<Tweet> tweets = new ArrayList<Tweet>();
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		
		// get all the tweet for the user id and order them by the timestamp desc
		Query q =
			    new Query("Tweet")
			        .setFilter(new FilterPredicate("user_id", FilterOperator.EQUAL, user_id))
			        .addSort("timestamp", SortDirection.DESCENDING);

		PreparedQuery results = datastore.prepare(q);
		
		// iterate through the results and create a list of tweet objects
		for (Entity result : results.asIterable()) {
			
			// if this is for the friends page we want to mark the tweet 
			// as viewed for the top tweets page order. 
			if (markVisited) {
				Long visited = (Long) result.getProperty("visited");
				result.setProperty("visited", visited + 1);
				datastore.put(result);
			}
			// create the tweet object and add it the the tweets list.
			tweets.add(
					new Tweet(
							(Long) result.getKey().getId(),
							(String) result.getProperty("content"),
							(String) result.getProperty("user_id"),
							(String) result.getProperty("user_name"),
							(Date) result.getProperty("timestamp"),
							(Long) result.getProperty("visited")
							)
					);
		}
		
		return tweets;
	}
	
	
	// delete takes a tweet id and deletes it from storage.
	public static void delete(String tweet_id) {
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Key key = KeyFactory.createKey("Tweet", Long.parseLong(tweet_id)); 
		datastore.delete(key);
	}

	
	// TopTweets returns all the tweets in order of popularity.
	public static ArrayList<Tweet> TopTweets() {
		ArrayList<Tweet> tweets = new ArrayList<Tweet>();
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		// get all the tweets from the database in order of most viewed.
		Query q = new Query("Tweet")
			        .addSort("visited", SortDirection.DESCENDING);
		PreparedQuery results = datastore.prepare(q);
		
		// create the tweet object and add it the the tweets list for every result
		for (Entity result : results.asIterable()) {
			tweets.add(
					new Tweet(
							(Long) result.getKey().getId(),
							(String) result.getProperty("content"),
							(String) result.getProperty("user_id"),
							(String) result.getProperty("user_name"),
							(Date) result.getProperty("timestamp"),
							(Long) result.getProperty("visited")
							)
					);
		}
		
		return tweets;
	}
	
	
	/* Constructors */
	
	// takes all the models parameters returns a tweet object
	public Tweet(Long id, String content, String user_id, String user_name, Date timestamp, Long visited ) {
		super();
		this.content = content;
		this.user_id = user_id;
		this.user_name = user_name;
		this.timestamp = timestamp;
		this.visited = visited;
		this.id = id;
	}
	
	// takes most the models parameters returns a tweet object but uses the current time for the timestamp
	// and 0 for the views.  Used when creating a tweet from the form submittion.
	public Tweet(String content, String user_id, String user_name) {
		super();
		this.content = content;
		this.user_id = user_id;
		this.user_name = user_name;
		this.timestamp = new Date(); 
		this.visited = 0l;
	}
	
	// Creates a new tweet object from the data in storage based on the tweet id
	public Tweet(String tweet_id) throws EntityNotFoundException {
		// get the tweet from storage
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Key key = KeyFactory.createKey("Tweet", Long.parseLong(tweet_id)); 
		Entity storedTweet = datastore.get(key);
		
		// load up all the properties from the data collected.
		this.id = Long.parseLong(tweet_id);
		this.content = (String) storedTweet.getProperty("content");
		this.user_id = (String) storedTweet.getProperty("user_id");
		this.user_name = (String) storedTweet.getProperty("user_name");
		this.timestamp = (Date) storedTweet.getProperty("timestamp");
		this.visited = (Long) storedTweet.getProperty("visited");
	}

	// Saves a tweet object to the database
	public void save() {
		
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		// create the tweet Entity
		Entity tweetStore = new Entity("Tweet");
		tweetStore.setProperty("content", this.content);
		tweetStore.setProperty("user_id", this.user_id);
		tweetStore.setProperty("user_name", this.user_name);
		tweetStore.setProperty("timestamp", this.timestamp);
		tweetStore.setProperty("visited", this.visited);
		// store it saving the generated id
		this.id = datastore.put(tweetStore).getId();
		
	}
	
	
	// getters 

	public Long getId() {
		return id;
	}
	
	public String getContent() {
		return content;
	}

	public String getUser_id() {
		return user_id;
	}

	public String getUser_name() {
		return user_name;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public Long getVisited() {
		return visited;
	}

	
	

	
	
}
