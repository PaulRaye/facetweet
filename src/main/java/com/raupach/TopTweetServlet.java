package com.raupach;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.raupach.models.Tweet;

@WebServlet(
    name = "TopTweetServlet",
    urlPatterns = {"/top-tweets"}
)

/*
 * The Top Tweet Servlet collects all the tweets, in order of popularity
 * and hands them to the top_tweets.jsp view.
 */


public class TopTweetServlet extends HttpServlet {

  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response) 
      throws IOException {
	  
	// collect the top tweets and hand them to the view
	ArrayList<Tweet> tweets = Tweet.TopTweets();
	
	request.setAttribute("tweets", tweets);
	
	try {
		request.getRequestDispatcher("top_tweets.jsp").forward(request, response);
	} catch (ServletException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

  }
}