<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Log-in</title>

<link rel="stylesheet" href="/css/styles.css">
</head>
<body>
	<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '105089530374875',
      cookie     : true,
      xfbml      : true,
      version    : 'v3.0'
    });
      
    FB.AppEvents.logPageView();   
      
    FB.getLoginStatus(function(response) {
	    statusChangeCallback(response);
	});
  
  };
  

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
  
  function checkLoginState() {
	  FB.getLoginStatus(function(response) {
	    statusChangeCallback(response);
	  });
	}
  
  
  // if the user is connected then forward them to the tweet page 
  // along with all thier user data we collect from facebook
  function statusChangeCallback(response) {
	  if (response.status == 'connected') {

		  FB.api('/me?fields=first_name,last_name,email', function(data) {
			  var uid = data.id;
			  var first_name = data.first_name;
			  var last_name = data.last_name;
			  var email_address  = data.email;
			  
			  window.location.replace("/tweet?userID=" + uid + 
					  "&first_name=" + first_name +
					  "&last_name=" + last_name +
					  "&email_address=" + email_address);
          });
		  
   		} else {
   			
        }
	}

</script>

<fb:login-button 
  scope="public_profile,email,friends"
  onlogin="checkLoginState();">
</fb:login-button>

</body>
</html>