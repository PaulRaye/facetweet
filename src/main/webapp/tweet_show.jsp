<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.raupach.models.Tweet" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


<meta property="fb:app_id" content="105089530374875">
<meta property="og:type"   content="article" />
<meta property="og:title" content="Tweet by ${ tweet.user_name }">
<meta property="og:description" content="${ tweet.content }">
<meta property="og:image" content="https://s3-us-west-1.amazonaws.com/4pr.solutions/images/bannersaga_cypress_512.png">
<meta property="og:image:width" content="512">
<meta property="og:image:height" content="512">
<title>Tweet by ${ tweet.user_name }</title>

<link rel="stylesheet" href="/css/styles.css">

</head>
<body>
	<jsp:include page="_facebook.jsp" />
	<div class="topnav">
  		<a class="active" href="/tweet">Tweet</a>
  		<a id="friends-nav-button" href="/friends">Friends</a>
  		<a href="/top-tweets">Top Tweets</a>
	</div>
	
	<div class="tweet">
	<span class="content">${ tweet.content } </span> <br>
	<span class="author">${ tweet.user_name } </span> 
	<span class="timestamp">${ tweet.timestamp }</span> <br>
	</div>

	
</body>
</html>