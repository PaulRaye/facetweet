<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.raupach.models.User" %>
<%@page import="com.raupach.models.Tweet" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>

<link rel="stylesheet" href="/css/styles.css">
</head>
<body>
	<jsp:include page="_facebook.jsp" />
	<div class="topnav">
  		<a href="/tweet">Tweet</a>
  		<a id="friends-nav-button" class="active" href="/friends">Friends</a>
  		<a href="/top-tweets">Top Tweets</a>
	</div>
	
	
		<div class="tweet-list">
	<% ArrayList<User> friends = (ArrayList<User>) request.getAttribute("friends");
	
	for (User friend : friends) {
	out.print("<h2>" + friend.getFullName() + "</h2>");
	
	ArrayList<Tweet> tweets = (ArrayList<Tweet>) friend.getTweets();
	
	for (Tweet tweet : tweets) {
		out.print("<div class=\"tweet\">");
		out.print("<span class=\"content\">" + tweet.getContent() + "</span> <br>");
		out.print("<span class=\"author\">" + tweet.getUser_name() + "</span> ");
		out.print("<span class=\"timestamp\">" + tweet.getTimestamp() + "</span> <br>");
		out.print("<div class=\"btn-bar\">");
		out.print("<a href=\"/tweet/show?tweetID=" + tweet.getId() + "\">View</a> ");
		out.print("<a data-id=\"" + tweet.getId() + "\" " +  
					 "data-content=\"" + tweet.getContent() + "\" " + 
				     "data-author=\"" + tweet.getUser_name() + "\" " + 
		             "class=\"share_tweet\" href=\"/tweet/show?tweetID=" + tweet.getId() + "\">Share</a> ");
		out.print("<a data-id=\"" + tweet.getId() + "\" " +  
				 "data-content=\"" + tweet.getContent() + "\" " + 
			     "data-author=\"" + tweet.getUser_name() + "\" " + 
	             "class=\"send_tweet\" href=\"/tweet/show?tweetID=" + tweet.getId() + "\">Send</a> ");
	
		out.print("</div></div>");
	}
	}
	%>
	</div>
	
	
	
	
</body>
</html>