<script>
  window.fbAsyncInit = function() {
	    FB.init({
	        appId      : '105089530374875',
	        xfbml      : true,
	        version    : 'v3.0'
	      });
      
    FB.AppEvents.logPageView();   
      
    FB.getLoginStatus(function(response) {
	   // statusChangeCallback(response);
	});
    
    
    // put a listener on each of the share buttons 
    var classname = document.getElementsByClassName("share_tweet");
    var share_tweet = function(event) {
    	event.preventDefault()
    	
        // Call the faceback share method to share our tweet to to a timeline
        FB.ui({
  		  method: 'share',
  		  href: 'https://facetweet-204615.appspot.com/tweet/show?tweetID=' + this.dataset.id,
  		  quote: this.dataset.content + " --" + this.dataset.author,
  		  app_id: '105089530374875'
  		}, function(response){});
    };

    for (var i = 0; i < classname.length; i++) {
        classname[i].addEventListener('click', share_tweet, false);
    };
    
    
    // put a listener on each of the send buttons 
    var classname = document.getElementsByClassName("send_tweet");
    var send_tweet = function(event) {
    	event.preventDefault()
        
    	// Call the facebook send method to messenge a user with a tweet 
        FB.ui({
  		  method: 'send',
  		  link: 'https://facetweet-204615.appspot.com/tweet/show?tweetID=' + this.dataset.id,
  		  app_id: '105089530374875'
  		}, function(response){});
    };

    for (var i = 0; i < classname.length; i++) {
        classname[i].addEventListener('click', send_tweet, false);
    };
    
    
    // put a listenser on the friends navigation button that collects the user's friends 
    // ids and sends them the the server
    document.getElementById("friends-nav-button").addEventListener("click", function(event){
        event.preventDefault();
        
        // call the facebook friends API method
        FB.api(
        		  '/me/friends',
        		  'GET',
        		  {},
        		  function(response) {
					  // gather the friend ids 
        		      var friends = []
        		      var arrayLength = response.data.length;
        		      for (var i = 0; i < arrayLength; i++) {
        		          friends.push(response.data[i].id);
        		      }
        		      // pass them to the server loading up a new page
        		      window.location.replace("/friends?friends=" + friends);
        		  }
        		);
    });
    
  
  };
  
  
  

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
  
  
  

</script>