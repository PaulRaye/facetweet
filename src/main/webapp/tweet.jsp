<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.raupach.models.Tweet" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Tweet</title>

<link rel="stylesheet" href="/css/styles.css">

</head>
<body>
	<jsp:include page="_facebook.jsp" />
	<div class="topnav">
  		<a class="active" href="/tweet">Tweet</a>
  		<a id="friends-nav-button" href="/friends">Friends</a>
  		<a href="/top-tweets">Top Tweets</a>
	</div>


	${ user.fullName }
	
	<div>
	<form method="POST" action="/tweet">
		<input id="tweet-field" type="text" name="content">
		<input type="submit" value="Tweet">
		<input type="hidden" name="userID" value="${user.id}">
	</form>
	</div> 
	<div class="tweet-list">
	<% ArrayList<Tweet> tweets = (ArrayList<Tweet>) request.getAttribute("tweets");
	
	for (Tweet tweet : tweets) {
		out.print("<div class=\"tweet\">");
		out.print("<span class=\"content\">" + tweet.getContent() + "</span> <br>");
		out.print("<span class=\"author\">" + tweet.getUser_name() + "</span> ");
		out.print("<span class=\"timestamp\">" + tweet.getTimestamp() + "</span> <br>");
		out.print("<div class=\"btn-bar\">");
		out.print("<a href=\"/tweet/delete?tweetID=" + tweet.getId() + "\">Delete</a> ");
		out.print("<a href=\"/tweet/show?tweetID=" + tweet.getId() + "\">View</a> ");
		//out.print("<a onclick=\"share_tweet(" + tweet.getId() + ", '" + tweet.getContent() + "')\" class=\"share_tweet\" href=\"/tweet/show?tweetID=" + tweet.getId() + "\">Share</a>");
		out.print("<a data-id=\"" + tweet.getId() + "\" " +  
					 "data-content=\"" + tweet.getContent() + "\" " + 
				     "data-author=\"" + tweet.getUser_name() + "\" " + 
		             "class=\"share_tweet\" href=\"/tweet/show?tweetID=" + tweet.getId() + "\">Share</a> ");
		out.print("<a data-id=\"" + tweet.getId() + "\" " +  
				 "data-content=\"" + tweet.getContent() + "\" " + 
			     "data-author=\"" + tweet.getUser_name() + "\" " + 
	             "class=\"send_tweet\" href=\"/tweet/show?tweetID=" + tweet.getId() + "\">Send</a> ");
	
		out.print("</div></div>");
	}
	
	%>
	</div>
</body>
</html>